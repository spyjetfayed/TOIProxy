package com.sjfd.myapplication

import android.os.Bundle
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

// program start
class MainActivity : AppCompatActivity() {

    private lateinit var urlbox: TextView
    private lateinit var toiWebView: WebView
    private val toiUrl = "https://m.timesofindia.com"       // default url for times of india
    private val proxy = "https://abcnews.com"               // proxy

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // create webview to display webpages
        toiWebView = findViewById(R.id.web_toiproxy)
        toiWebView.webViewClient = ToiWebViewClient()       // add webview client
        toiWebView.loadUrl(toiUrl)                          // load the TOI homepage

        // create textview for displaying url
        urlbox = findViewById(R.id.text_url)
        urlbox.text = proxy                                 // update the url
    }

    private inner class ToiWebViewClient: WebViewClient() {

        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            urlbox.text = request?.url.toString().replace(toiUrl, proxy)        // replace the TOI url with proxy
            return super.shouldOverrideUrlLoading(view, request)
        }
    }
}
